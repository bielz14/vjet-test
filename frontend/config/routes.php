<?php
$urlRouteClass = 'ancor\rest\UrlRule';
return [
    '' => 'site/posts',

    'post/<id:\d+>' => 'site/post',
    'post/add' => 'site/addpost',

    'fast/add' => 'site/addfast',
];