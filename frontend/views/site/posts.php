<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PostForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$monthsList = [
    "1" => "January", "2" => "February", "3" => "March",
    "4" => "April", "5" => "May", "6" => "June",
    "7" => "July", "8" => "August", "9" => "September",
    "10" => "October", "11" => "November", "12" => "December"
];

?>
<?php $session = Yii::$app->session; ?>
<?php if ($session->has('success')): ?>
    <div id="adding-message" style="background: #00cc66;">
        <span style="color: white">&nbsp Comment on publication was successfully added.</span>
    </div>
    <script type="text/javascript">
        setTimeout(function(){
            $('#adding-message').remove();
        }, 2500);
    </script>
    <?php $session->destroy(); ?>
<?php endif; ?>
<div id="main-content">
    <?php if (isset($postsByMaxFasts)): ?>
        <div class='slide' style="margin-left: 33%">
            <h2 style="margin-top: 1%">Posts by max fasts</h2>
            <?php foreach ($postsByMaxFasts as $key => $post): ?>
                <input type="radio" name="slider2" id="slider2_<?= $key + 1 ?>" checked="checked">
                <label for="slider2_<?= $key + 1 ?>"></label>
                <div style="margin-top: 15%">
                    <span style="color: #888; font-size: 17px;">
                        <?= $monthsList[date('n', $post->created_at)] ?>&nbsp<?= date('d, Y', $post->created_at);  ?>
                        &nbsp
                        by <?= $post->owner_name ?>
                    </span>
                    <span class="comments-count" style="display: block">
                        comments (<?= count($post->fasts) ?>)
                    </span>
                    <p><?= mb_strimwidth($post->description, 0, 100, '...'); ?></p>
                    <center><a class="button " href="/post/<?= $post->id ?>">Read More</a></center>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <div class="wrap-content" style="display: inline-block; margin-left: 18%;">
        <h2>Recent posts</h2>
        <?php if (isset($posts) && $posts): ?>
            <div class="row">
                <?php foreach ($posts as $post): ?>
                    <article class="single-post zerogrid">
                        <div class="row wrap-post"><!--Start Box-->
                            <div class="entry-header">
                                <span class="time">
                                    <?= $monthsList[date('n', $post->created_at)] ?>&nbsp<?= date('d, Y', $post->created_at);  ?>
                                    &nbsp
                                    by <?= $post->owner_name ?>
                                </span>
                                <span class="comments-count">
                                    comments (<?= count($post->fasts) ?>)
                                </span>
                            </div>
                            <div class="entry-content">
                                <p><?= mb_strimwidth($post->description, 0, 100, '...'); ?></p>
                                <center><a class="button " href="/post/<?= $post->id ?>">Read More</a></center>
                            </div>
                        </div>
                    </article>
                <?php endforeach; ?>
            </div>
        <?php else: ?>
           <span style="margin-right: 1%; font-size: 300%; color: #FF5733">Don't find to posts.</span>
        <?php endif; ?>
    </div>
    <div class="post-form" style="display: inline-block; margin-left: 3%; width: 25%">

        <?php $form = ActiveForm::begin(['action' => '/post/add', 'id' => 'post-add-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>
        <?=  Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []); ?>
        <div class="row">
            <div class="col-md-6">
                <div class="box box-primary " style="border: none; border-top:solid 3px #3c8dbc;">
                    <div class="box-body ">
                        <div class="form-group field-post-description">
                            <?= $form->field($model, 'description')->textArea(['class' => 'form-control', 'name' => 'description', 'required' => ''])->label('description *'); ?>
                        </div>
                        <div class="form-group field-post-owner_name">
                            <?= $form->field($model, 'owner_name')->input('text', ['class' => 'form-control', 'name' => 'owner_name', 'required' => ''])->label('author *'); ?>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Adding</button> </div>
                    </div>
                </div>
            </div>

        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>