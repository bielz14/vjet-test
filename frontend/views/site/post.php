<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PostForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Post';
$this->params['breadcrumbs'][] = $this->title;

$monthsList = [
    "1" => "January", "2" => "February", "3" => "March",
    "4" => "April", "5" => "May", "6" => "June",
    "7" => "July", "8" => "August", "9" => "September",
    "10" => "October", "11" => "November", "12" => "December"
];

?>
<?php $session = Yii::$app->session; ?>
<?php if ($session->has('success')): ?>
    <div id="adding-message" style="background: #00cc66;">
        <span style="color: white">Publication was successfully added.</span>
    </div>
    <script type="text/javascript">
        setTimeout(function(){
            $('#adding-message').remove();
        }, 2500);
    </script>
    <?php $session->destroy(); ?>
<?php endif; ?>
<div class="psot-view">
    <?php if (isset($post) && $post): ?>
        <section id="container">
            <div class="wrap-container">
                <div id="main-content">
                    <div class="wrap-content">
                        <div class="row">
                            <article class="single-post zerogrid">
                                <div class="row wrap-post">
                                    <div class="entry-header">
                                        <span class="time">
                                        <?= $monthsList[date('n', $post->created_at)] ?>&nbsp<?= date('d, Y', $post->created_at);  ?>
                                        &nbsp by &nbsp<?= $post->owner_name ?>
                                        </span>
                                    </div>
                                    <div class="entry-content">
                                        <p><?= $post->description ?></p>
                                    </div>
                                </div>
                                <?php if ($post->fasts): ?>
                                    <div id="comments">
                                        <ol class="commentlist">
                                            <?php foreach ($post->fasts as $fast): ?>
                                                <li class="comment">
                                                    <div class="comment-body">
                                                        <div class="comment-wrapper">
                                                            <div class="comment-meta" style="margin-bottom:5px;">
                                                                <span class="fn">
                                                                    <?= $monthsList[date('n', $post->created_at)] ?>&nbsp<?= date('d, Y', $post->created_at);  ?>
                                                                     &nbsp by &nbsp<?= $fast->owner_name ?>
                                                                </span>
                                                            </div>
                                                            <div class="hentry-content">
                                                                <?= $fast->text ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            <?php endforeach; ?>
                                        </ol>
                                    </div>
                                <?php endif; ?>
                            </article>
                            <div class="zerogrid">
                                <div class="comments-are">
                                    <div id="comment">
                                        <?php $form = ActiveForm::begin(['action' => '/fast/add', 'id' => 'fast-add-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>
                                        <?= Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []); ?>
                                        <?= $form->field($post, 'id')->input('hidden', ['class' => 'form-control', 'name' => 'post_id'])->label(false); ?>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="box box-primary " style="border: none; border-top:solid 3px #3c8dbc;">
                                                    <div class="box-body ">
                                                        <div class="form-group field-fast-description">
                                                            <?= $form->field($model, 'text')->textArea(['class' => 'form-control', 'name' => 'text', 'required' => ''])->label('comment *'); ?>
                                                        </div>
                                                        <div class="form-group field-fast-owner_name">
                                                            <?= $form->field($model, 'owner_name')->input('text', ['class' => 'form-control', 'name' => 'owner_name', 'required' => ''])->label('author *'); ?>
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-primary">Adding</button> </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <?php ActiveForm::end(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php else: ?>
            <span style="margin: 21% 0 0 39%; font-size: 300%; color: #FF5733">Don't missing to post.</span>
    <?php endif; ?>
</div>