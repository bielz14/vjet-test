<?php
namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * Post form
 */
class PostForm extends Model
{
    public $id;
    public $description;
    public $owner_name;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['id', 'integer'],

            ['description', 'required'],
            ['description', 'trim'],
            ['description', 'string', 'min' => 2],

            ['owner_name', 'required'],
            ['owner_name', 'string', 'min' => 2],
        ];
    }

    public function addPost()
    {
        if (!$this->validate()) {
            return $this->validate();
        }

        $post = new Post();
        $post->description = $this->description;
        $post->owner_name = $this->owner_name;

        return $post->save();
    }
}
