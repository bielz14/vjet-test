<?php
namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * Fast form
 */
class FastForm extends Model
{
    public $id;
    public $text;
    public $owner_name;
    public $post_id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['id', 'integer'],

            ['text', 'required'],
            ['text', 'trim'],
            ['text', 'string', 'min' => 1],

            ['owner_name', 'required'],
            ['owner_name', 'string', 'min' => 2],

            ['post_id', 'required'],
            ['post_id', 'integer'],
        ];
    }

    public function addFast()
    {
        if (!$this->validate()) {
            return null;
        }

        $fast = new Fast();

        $fast->text = $this->text;
        $fast->owner_name = $this->owner_name;
        $fast->post_id = $this->post_id;
        if ($fast->save()) {
            return $fast;
        } else {
            return false;
        }
    }
}
