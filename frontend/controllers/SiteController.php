<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\Post;
use frontend\models\PostForm;
use frontend\models\Fast;
use frontend\models\FastForm;
use yii\helpers\Url;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['posts', 'addpost', 'addfast'],
                'rules' => [
                    [
                        'actions' => ['posts', 'addpost', 'addfast'],
                        'allow' => true,
                        'roles' => ['?', '@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'posts' => ['get'],
                    'addpost' => ['post'],
                    'addfast' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionPosts()
    {
        $secondInDay = 60 * 60 * 24;
        $dayAgo = time() - $secondInDay;
        $posts = Post::find()
            ->where(['>', 'created_at', $dayAgo])
            ->orderBy(['created_at' => SORT_DESC])
            ->all();//Все публикации за последние сутки

        $model = new Post();

        $postsAll = Post::find()
            ->all();
        foreach ($postsAll as $key => $value) {
            foreach ($postsAll as $key2 => $value2) {
                if ($value->id > $value2->id) {//if (count($value->fasts) < count($value2->fasts)) {
                    $oldData = $postsAll[$key2];
                    $postsAll[$key2] = $postsAll[$key];
                    $postsAll[$key] = $oldData;
                }
            }
        }
        foreach ($postsAll as $key => $post) {
            if ($key > 4) {
                unset($postsAll[$key]);
            }
        }
        $postsByMaxFasts = $postsAll;

        return $this->render('posts', [
            'posts' => $posts,
            'model' => $model,
            'postsByMaxFasts' => $postsByMaxFasts
        ]);
    }

    public function actionPost()
    {
        $post = Post::findOne(['id' => Yii::$app->request->get('id')]);

        $model = new Fast();

        return $this->render('post', [
            'post' => $post,
            'model' => $model
        ]);
    }

    public function actionAddpost()
    {
        $model = new PostForm();
        if ($model->load(Yii::$app->request->post(),'')) {
            if ($model->addPost()) {
                $session = Yii::$app->session;
                if (!$session->isActive) {
                    $session->open();
                }
                $session->set('success', true);
                return $this->goHome();
            }
        }

        $secondInDay = 60 * 60 * 24;
        $dayAgo = time() - $secondInDay;
        $posts = Post::find()
            ->where(['>', 'created_at', $dayAgo])
            ->orderBy(['created_at' => SORT_DESC])
            ->all();//Все публикации за последние сутки

        $postsAll = Post::find()
            ->all();
        foreach ($postsAll as $key => $value) {
            foreach ($postsAll as $key2 => $value2) {
                if ($value->id > $value2->id) {//if (count($value->fasts) < count($value2->fasts)) {
                    $oldData = $postsAll[$key2];
                    $postsAll[$key2] = $postsAll[$key];
                    $postsAll[$key] = $oldData;
                }
            }
        }
        foreach ($postsAll as $key => $post) {
            if ($key > 4) {
                unset($postsAll[$key]);
            }
        }
        $postsByMaxFasts = $postsAll;

        return $this->render('posts', [
            'posts' => $posts,
            'model' => $model,
            'postsByMaxFasts' => $postsByMaxFasts
        ]);
    }

    public function actionAddfast()
    {
        $model = new FastForm();
        if ($model->load(Yii::$app->request->post(),'')) {
            if ($fast = $model->addFast()) {
                $session = Yii::$app->session;
                if (!$session->isActive) {
                    $session->open();
                }
                $session->set('success', true);
                return Yii::$app->response->redirect(Url::to('/post/' . $fast->post_id));
            }
        }

        $post = Post::findOne(['id' => Yii::$app->request->post('post_id')]);
        return $this->render('post', [
            'post' => $post,
            'model' => $model
        ]);
    }
}
