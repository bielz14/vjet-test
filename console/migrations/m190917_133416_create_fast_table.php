<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%fast}}`.
 */
class m190917_133416_create_fast_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%fast}}', [
            'id' => $this->primaryKey(),
            'text' => $this->text()->notNull(),
            'owner_name' => $this->string()->notNull(),
            'post_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->createIndex(
            'idx-fast-post_id',
            'fast',
            'post_id'
        );

        $this->addForeignKey(
            'fk-fast-post_id',
            'fast',
            'post_id',
            'post',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%fast}}');
    }
}
